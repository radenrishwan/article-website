import z from "zod";

const loginSchema = z.object({
    email: z.string().email({
        message: "Please enter a valid email address.",
    }).max(100),
    password: z.string().min(6).max(100),
})

export default loginSchema