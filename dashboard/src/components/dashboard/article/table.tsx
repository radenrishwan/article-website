import { Table, TableBody, TableCaption, TableCell, TableHead, TableHeader, TableRow } from "@/components/ui/table";
import { Article } from "@/model/article";

type ArticleTableProps = {
    articles: Article[],
}

export function ArticleTable({ articles }: ArticleTableProps) {
    const buildStatus = (status: string) => {
        if (status === 'deleted') {
        return <TableCell className=""><p className="bg-red-400 p-2 rounded-md w-[5rem] text-center text-white">{status}</p></TableCell>
        }

        if (status === 'draft') {
            return <TableCell className=""><p className="bg-primary p-2 rounded-md w-[5rem] text-center text-white">{status}</p></TableCell>
        }

        return <TableCell className=""><p className="bg-green-400 p-2 rounded-md w-[5rem] text-center text-white">{status}</p></TableCell>
    }

    return (
        <Table>
            <TableCaption>Article list</TableCaption>
            <TableHeader>
                <TableRow>
                    <TableHead className="w-[100px]">Title</TableHead>
                    <TableHead>Content</TableHead>
                    <TableHead className="text-center">Created At</TableHead>
                    <TableHead className="text-center">Updated At</TableHead>
                    <TableHead className="text-center">Status</TableHead>
                </TableRow>
            </TableHeader>
            <TableBody className="">
                {articles.map((article) => (
                    <TableRow key={article.id}>
                        <TableCell className="font-medium">{article.title}</TableCell>
                        <TableCell className="line-clamp-1">{article.content}</TableCell>
                        <TableCell className="text-center w-[8rem]">{article.createdAt}</TableCell>
                        <TableCell className="text-center w-[8rem]">{article.updatedAt}</TableCell>
                        {buildStatus(article.status)}
                    </TableRow>
                ))}
            </TableBody>
        </Table>
    )
}