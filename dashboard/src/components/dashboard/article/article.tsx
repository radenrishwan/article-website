import { Separator } from "@/components/ui/separator"
import { ArticleTable } from "./table"
import { Article } from "@/model/article"

const dummyArticle: Article[] = [
    {
        id: "123",
        content: "Hello, World",
        title: "Hello",
        slug: "hello",
        status: 'release',
        createdAt: "2 days ago",
        updatedAt: "now",
    },
    {
        id: "123",
        content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ultricies efficitur leo et dictum. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Praesent molestie suscipit nisl, sit amet hendrerit mi sagittis ut. Pellentesque fermentum nisi rhoncus maximus ornare. Proin iaculis turpis sit amet lacus facilisis, ut aliquet tortor ultrices. Proin eu tincidunt arcu, eu lacinia magna. Quisque non metus egestas, egestas nulla nec, fermentum justo. Praesent interdum turpis sed erat finibus suscipit. Vivamus ac urna metus. Donec sed metus massa. Sed ultricies malesuada nibh semper mattis. In ultricies consequat elit, in faucibus dolor fermentum vel. Duis ullamcorper, nisl non accumsan tempus, eros sem ultrices elit, at lacinia sem arcu nec erat. Suspendisse placerat sem sit amet velit fringilla, eget feugiat orci blandit.",
        title: "Hello",
        slug: "hello",
        status: 'draft',
        createdAt: "2 days ago",
        updatedAt: "now",
    },
    {
        id: "123",
        content: "Hello, World",
        title: "Hello",
        slug: "hello",
        status: 'deleted',
        createdAt: "2 days ago",
        updatedAt: "now",
    }
]

export function ArticleView() {
    return (
        <div className="flex flex-col">
            <p className="text-xl font-medium">Article list</p>
            <Separator className="my-4" />
            <ArticleTable articles={dummyArticle}/>
        </div>
    )
}