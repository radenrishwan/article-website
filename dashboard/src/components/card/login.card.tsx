import { Card, CardContent, CardHeader, CardTitle } from "../ui/card";
import { LoginForm } from '@/components/form/login.form'

export default function LoginComponent() {
    return < Card className='w-[30%]' >
        <CardHeader>
            <CardTitle>Login to continue</CardTitle>
        </CardHeader>
        <CardContent>
            <LoginForm />
        </CardContent>
    </Card >
}