import Image from 'next/image'
import { Separator } from "@/components/ui/separator"

type SidebarProps = {
    children: React.ReactNode
}

type SidebarItemProps = {
    children: React.ReactNode
    onClick: () => void
}


export function Sidebar({ children }: SidebarProps) {
    return (
        <aside className="h-screen w-[18rem] bg-primary p-2 sticky left-0 top-0">
            <nav className="h-full flex flex-col shadow-sm">
                <div className="p-4 pb-2 flex gap-4 items-center text-white">
                    <Image src="https://img.logoipsum.com/234.svg" alt="" width={50} height={50} />
                    <p>Article Dashboard</p>
                </div>
                <Separator className='my-4 '/>
                <ul className='flex-1 px-3'>{ children }</ul>
            </nav>
        </aside>
    )
}

export function SidebarItem({ children, onClick}: SidebarItemProps) {
    return (
        <li className="p-4 flex gap-4 items-center text-white cursor-pointer hover:bg-gray-300  rounded-md" onClick={onClick}>
            { children }
        </li>
    )
}