export type Article = {
    id: string,
    slug: string,
    title: string,
    content: string,
    status: "draft" | "release" | "deleted"
    createdAt: string, 
    updatedAt: string, 
}
