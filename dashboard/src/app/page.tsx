'use client'

import LoginComponent from '@/components/card/login.card'
import { RecoilRoot, atom} from 'recoil'

export const isSuccess = atom({
  key: 'isSuccess',
  default: false,
})

export default function Home() {

  return (
    <RecoilRoot>
       <main className="w-screen h-screen bg-background flex items-center justify-center">
        <LoginComponent />
      </main>
    </RecoilRoot>
  )
}
