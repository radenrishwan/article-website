import type { Metadata } from 'next'
import './globals.css'
import { Toaster } from '@/components/ui/toaster'

export const metadata: Metadata = {
  title: 'Article dahsboard',
  description: 'dashboard for article website by @radenrishwan',
  creator: '@radenrishwan',

}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <body>
          {children}
        <Toaster />
      </body>
    </html>
  )
}
