'use client'

import { Sidebar, SidebarItem } from "@/components/ui/sidebar";
import { icons } from "lucide-react"
import Editor from "@/components/dashboard/editor/editor";
import { useState } from "react";
import { ArticleView } from "@/components/dashboard/article/article";
import { useRouter } from "next/navigation";
import { toast } from "@/components/ui/use-toast";
import { atom } from "recoil";
import { Article } from "@/model/article";

type TabActive = "article" | "editor"

const editorState = atom<Article>({
    key: 'editorState',
    default: {
        id: '',
        title: '',
        slug: '',
        content: '',
        status: 'draft',
        createdAt: '',
        updatedAt: '',
    }
})

export default function Dashboard() {
    const router = useRouter()
    const [loading, setLoading] = useState(false)
    const [active, setActive] = useState<TabActive>("article")

    const renderTab = () => {
        if (active === 'article') {
            return <ArticleView />
        }

        return <Editor />
    }

    return (
        <>
            <main className="flex">
                <Sidebar>
                    <SidebarItem onClick={() => {
                        setActive('article')
                    }}>
                        <icons.StickyNote size={20} />
                        <p>Article</p>
                    </SidebarItem>
                    <SidebarItem onClick={() => {
                        setActive('editor')
                    }}>
                        <icons.FileEdit size={20} />
                        <p>Editor</p>
                    </SidebarItem>
                    <SidebarItem onClick={() => {
                        setLoading(true)
                        // delete token from cookie
                        // redirect to login page
                        setTimeout(() => {
                            setLoading(false)
                            toast({
                                title: "Logout success",
                                description: "You will be redirected to login page in 2 seconds",
                            })
                            setTimeout(() => {
                                router.push('/')
                            }, 2000)
                        }, 1000)
                    }}>
                        {loading ? (
                            <icons.Loader className="animate-spin" />
                        ) : <icons.LogOut />}<p className="pl-[-2px]">Logout</p>
                    </SidebarItem>
                </Sidebar>
                <section id="body" className="p-4 w-full ">
                    {renderTab()}
                </section>
            </main>
        </>
    )
}

